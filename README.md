### How to run

Open the swagger-example as gradle project -> start it

Start monitoring tools: `docker-compose -f docker/grafana-compose.yml up -d`

Start elastic stack: `docker-compose -f docker/elastic.yml up -d`
If it doesn't start you have to set: `sudo sysctl -w vm.max_map_count=262144`

To run the application with the APM jar add the following VM arguments: `-javaagent:"D:\Users\Sven\Downloads\elastic-apm-agent-1.19.0.jar" -Delastic.apm.service_name=swagger-example -Delastic.apm.server_urls=http://localhost:8200 -Delastic.apm.application_packages=eu.balev`

Start k6: ` docker run --add-host=host.docker.internal:host-gateway -i loadimpact/k6 run --out influxdb=http://host.docker.internal:8086/k6db - <k6/test.js`

Start gatling -> gradle tasks -> gatlingRun (clean before each run, otherwise it does nothing)

