var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "19656",
        "ok": "19656",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6951",
        "ok": "6951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "611",
        "ok": "611",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "463",
        "ok": "463",
        "ko": "-"
    },
    "percentiles1": {
        "total": "501",
        "ok": "501",
        "ko": "-"
    },
    "percentiles2": {
        "total": "816",
        "ok": "816",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1362",
        "ok": "1362",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14539,
    "percentage": 74
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3606,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1511,
    "percentage": 8
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "322.23",
        "ok": "322.23",
        "ko": "-"
    }
},
contents: {
"req_request6-55a46": {
        type: "REQUEST",
        name: "request6",
path: "request6",
pathFormatted: "req_request6-55a46",
stats: {
    "name": "request6",
    "numberOfRequests": {
        "total": "3276",
        "ok": "3276",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6951",
        "ok": "6951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "592",
        "ok": "592",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1060",
        "ok": "1060",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1284",
        "ok": "1284",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3701",
        "ok": "3701",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 308,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1904,
    "percentage": 58
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1064,
    "percentage": 32
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "53.705",
        "ok": "53.705",
        "ko": "-"
    }
}
    },"req_request1-57888": {
        type: "REQUEST",
        name: "request1",
path: "request1",
pathFormatted: "req_request1-57888",
stats: {
    "name": "request1",
    "numberOfRequests": {
        "total": "3276",
        "ok": "3276",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4597",
        "ok": "4597",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "455",
        "ok": "455",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "percentiles1": {
        "total": "410",
        "ok": "410",
        "ko": "-"
    },
    "percentiles2": {
        "total": "600",
        "ok": "600",
        "ko": "-"
    },
    "percentiles3": {
        "total": "867",
        "ok": "867",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1489",
        "ok": "1489",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3016,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 215,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 45,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "53.705",
        "ok": "53.705",
        "ko": "-"
    }
}
    },"req_request2-134f7": {
        type: "REQUEST",
        name: "request2",
path: "request2",
pathFormatted: "req_request2-134f7",
stats: {
    "name": "request2",
    "numberOfRequests": {
        "total": "3276",
        "ok": "3276",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2525",
        "ok": "2525",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "452",
        "ok": "452",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "percentiles1": {
        "total": "410",
        "ok": "410",
        "ko": "-"
    },
    "percentiles2": {
        "total": "608",
        "ok": "608",
        "ko": "-"
    },
    "percentiles3": {
        "total": "862",
        "ok": "862",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1568",
        "ok": "1568",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3020,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 202,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 54,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "53.705",
        "ok": "53.705",
        "ko": "-"
    }
}
    },"req_reques3-9fa71": {
        type: "REQUEST",
        name: "reques3",
path: "reques3",
pathFormatted: "req_reques3-9fa71",
stats: {
    "name": "reques3",
    "numberOfRequests": {
        "total": "3276",
        "ok": "3276",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "488",
        "ok": "488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "323",
        "ok": "323",
        "ko": "-"
    },
    "percentiles1": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles2": {
        "total": "642",
        "ok": "642",
        "ko": "-"
    },
    "percentiles3": {
        "total": "999",
        "ok": "999",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1857",
        "ok": "1857",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2848,
    "percentage": 87
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 345,
    "percentage": 11
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 83,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "53.705",
        "ok": "53.705",
        "ko": "-"
    }
}
    },"req_request5-cdd0c": {
        type: "REQUEST",
        name: "request5",
path: "request5",
pathFormatted: "req_request5-cdd0c",
stats: {
    "name": "request5",
    "numberOfRequests": {
        "total": "3276",
        "ok": "3276",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "602",
        "ok": "602",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "percentiles1": {
        "total": "531",
        "ok": "531",
        "ko": "-"
    },
    "percentiles2": {
        "total": "808",
        "ok": "808",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2053",
        "ok": "2053",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2433,
    "percentage": 74
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 643,
    "percentage": 20
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 200,
    "percentage": 6
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "53.705",
        "ok": "53.705",
        "ko": "-"
    }
}
    },"req_request4-c9ae9": {
        type: "REQUEST",
        name: "request4",
path: "request4",
pathFormatted: "req_request4-c9ae9",
stats: {
    "name": "request4",
    "numberOfRequests": {
        "total": "3276",
        "ok": "3276",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2645",
        "ok": "2645",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "463",
        "ok": "463",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "percentiles1": {
        "total": "417",
        "ok": "417",
        "ko": "-"
    },
    "percentiles2": {
        "total": "631",
        "ok": "631",
        "ko": "-"
    },
    "percentiles3": {
        "total": "925",
        "ok": "925",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1638",
        "ok": "1638",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2914,
    "percentage": 89
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 297,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "53.705",
        "ok": "53.705",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
