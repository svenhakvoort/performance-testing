package perftest.advanced

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import io.gatling.http.request.builder.HttpRequestBuilder
import javax.jms._
import io.gatling.jms.Predef._
import io.gatling.jms.jndi.JmsJndiConnectionFactoryBuilder
import io.gatling.jms.protocol.JmsProtocolBuilder

import scala.concurrent.duration._
import scala.language.postfixOps

class JmsSimulation extends Simulation {

  // alternatively, you can create a ConnectionFactory from a JNDI lookup
  val jndiBasedConnectionFactory: JmsJndiConnectionFactoryBuilder = jmsJndiConnectionFactory
    .connectionFactoryName("ConnectionFactory")
    .url("tcp://localhost:61616")
    .credentials("user", "secret")
    .contextFactory("org.apache.activemq.jndi.ActiveMQInitialContextFactory")

  val jmsConfig: JmsProtocolBuilder = jms
    .connectionFactory(jndiBasedConnectionFactory)
    .usePersistentDeliveryMode

  val scn: ScenarioBuilder = scenario("JMS DSL test").repeat(1) {
    exec(jms("req reply testing").requestReply
      .queue("jmstestq")
      .textMessage("hello from gatling jms dsl")
      .property("test_header", "test_value")
      .jmsType("test_jms_type")
      .check(simpleCheck(checkBodyTextCorrect)))
  }

  setUp(scn.inject(rampUsersPerSec(10).to(1000).during(2.minutes)))
    .protocols(jmsConfig)

  def checkBodyTextCorrect(m: Message) = {
    // this assumes that the service just does an "uppercase" transform on the text
    m match {
      case tm: TextMessage => tm.getText == "HELLO FROM GATLING JMS DSL"
      case _               => false
    }
  }

}

