package perftest.advanced

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import io.gatling.http.request.builder.HttpRequestBuilder

import scala.concurrent.duration._
import scala.language.postfixOps

class ChecksSimulation extends Simulation {

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl("http://localhost:8080") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val scn: ScenarioBuilder = scenario("Test")
    .exec(Course.all)
    .exec(Student.create)

  setUp(
    scn.inject(constantConcurrentUsers(100) during (10 seconds)),
  ).protocols(httpProtocol)

  object Course {

    val all: HttpRequestBuilder = http("request6")
      .get("/api/courses")
      .check(status.is(_ => 200))

  }

  object Student {

    val create: HttpRequestBuilder =
      http("request1")
        .post("/api/students")
        .header("Content-Type", "application/json")
        .body(StringBody("""{"name":"test"}"""))
        .check(status.is(_ => 201))
        .check(header("location").transform(_.split("/").last).saveAs("studentId"))

  }
}
