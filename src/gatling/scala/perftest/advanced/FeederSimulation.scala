package perftest.advanced

import java.util.Random

import perftest.basic
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import io.gatling.http.request.builder.HttpRequestBuilder

import scala.concurrent.duration._
import scala.language.postfixOps

class FeederSimulation extends Simulation {

  val feeder: BatchableFeederBuilder[String]#F = csv("students.csv").random

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl("http://localhost:8080") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val scn: ScenarioBuilder = scenario("Test")
    .feed(feeder)
    .exec(Course.all)
    .exec(Student.create)
    .exec(Student.get)
    .exec(Student.update)
    .exec(Student.Course.addCourse)
    .exec(Student.Course.get)

  setUp(
    scn.inject(constantConcurrentUsers(100) during (10 seconds)),
  ).protocols(httpProtocol)

  object Course {

    val all: HttpRequestBuilder = http("request6")
      .get("/api/courses")
      .check(status.is(_ => 200))

  }

  object Student {

    val create: HttpRequestBuilder =
      http("request1")
        .post("/api/students")
        .header("Content-Type", "application/json")
        .body(StringBody("""{"name":"${studentName}"}"""))
        .check(status.is(_ => 201))
        .check(header("location").transform(_.split("/").last).saveAs("studentId"))

    val get: HttpRequestBuilder = http("request2")
      .get("/api/students/${studentId}")
      .check(status.is(_ => 200))
      .check(jsonPath("$.name").is("${studentName}"))

    val update: HttpRequestBuilder = http("reques3")
      .put("/api/students/${studentId}")
      .header("Content-Type", "application/json")
      .body(StringBody("""{"name":"${studentName}"}"""))
      .check(status.is(_ => 201))
      .check(jsonPath("$.name").is("${studentName}"))

    object Course {

      val get: HttpRequestBuilder = http("request4")
        .get("/api/students/${studentId}/courses")
        .check(status.is(_ => 200))
        .check(jsonPath("$[0].id").notNull)
        .check(jsonPath("$[0].name").is("${courseName}"))

      val addCourse: HttpRequestBuilder = http("request5")
        .patch("/api/students/${studentId}/courses")
        .header("Content-Type", "application/json")
        .body(StringBody("""[{"name":"${courseName}"}]"""))
        .check(status.is(_ => 204))

    }


  }
}


