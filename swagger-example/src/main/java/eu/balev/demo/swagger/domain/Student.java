package eu.balev.demo.swagger.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import io.swagger.annotations.ApiModelProperty;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@JacksonXmlRootElement
public class Student {

    @JacksonXmlProperty
	private int id;
    @JacksonXmlProperty
    private String name;
	private Collection<Course> courses;

	public Student() {

	}

	public Student(String name) {
		this.name = name;
	}

	public Student(String name, Collection<Course> courses) {
		this.name = name;
		this.courses = courses;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Collection<Course> getCourses() {
		return courses;
	}

	@ApiModelProperty(hidden=true)
	public void setCourses(Collection<Course> courses) {
		this.courses = courses;

	}
}
