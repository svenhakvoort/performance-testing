import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
    stages: [
        // { duration: '10s', target: 2000 }, // below normal load
        { duration: '1m', target: 4000 },
        // { duration: '2m', target: 200 }, // normal load
        // { duration: '5m', target: 200 },
        // { duration: '2m', target: 300 }, // around the breaking point
        // { duration: '5m', target: 300 },
        // { duration: '2m', target: 400 }, // beyond the breaking point
        // { duration: '5m', target: 400 },
        // { duration: '10m', target: 0 }, // scale down. Recovery stage.
    ],
};

export default function () {
    const BASE_URL = 'http://host.docker.internal:8080'; // make sure this is not production
    let responses = http.batch([
        [
            'GET',
            `${BASE_URL}/api/courses`,
            null,
            { tags: { name: 'Courses' } },
        ]
    ]);
    sleep(1);
}
